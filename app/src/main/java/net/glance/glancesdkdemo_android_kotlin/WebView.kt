package net.glance.glancesdkdemo_android_kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.webkit.WebView

class WebView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view)

        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings
        webView.loadUrl("https://ww2.glance.net")
    }
}
