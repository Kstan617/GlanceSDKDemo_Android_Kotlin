package net.glance.glancesdkdemo_android_kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import net.glance.android.Event
import net.glance.android.EventCode
import net.glance.android.EventType
import net.glance.android.Visitor

import net.glance.android.VisitorListener

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

// make sure your class implements VisitorListener
class MyActivity : AppCompatActivity(), VisitorListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my)

        // Initialize the Visitor with the group ID provided by Glance
        Visitor.init(this, 15687, "", "", "", "")
        // Make sure the Visitor is listening for events on this Activity
        Visitor.addListener(this)

        val startSessionBtn = findViewById<Button>(R.id.startSession)
        startSessionBtn.setOnClickListener {
            startSession()
        }
        val endSessionBtn = findViewById<Button>(R.id.endSession)
        endSessionBtn.setOnClickListener {
            endSession()
        }
        val openBrowserBtn = findViewById<Button>(R.id.openBrowser)
        openBrowserBtn.setOnClickListener {
            openBrowser()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        // Make sure to remove the listener before this Activity is destroyed
        Visitor.removeListener(this)
    }

    // this is called on the Glance Event thread
    override fun onGlanceVisitorEvent(event: Event) {
        if (event.code == EventCode.EventConnectedToSession) {
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here (sessionKey)
            // to display to the user to read to the agent
            val sessionKey = event.GetValue("sessionkey")
            setText(findViewById(R.id.textView2), sessionKey)
            toggleButtons()
        } else if (event.code == EventCode.EventSessionEnded) {
            // Show any UI to indicate the session has ended
            setText(findViewById(R.id.textView2), "Click button to below to start session")
            toggleButtons()
        } else if (event.type == EventType.EventWarning ||
                event.type == EventType.EventError ||
                event.type == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
            Log.d("EVENT_TYPE", event.type.toString() + " " + event.messageString)
        }
    }

    private fun setText(textView: TextView, value: String) {
        runOnUiThread { textView.text = value }
    }

    private fun toggleButtonVisibility(button: Button) {
        runOnUiThread {
            button.isEnabled = true
            if (button.visibility == View.VISIBLE) {
                button.visibility = View.GONE
            } else {
                button.visibility = View.VISIBLE
            }
        }
    }

    private fun toggleButtons() {
        toggleButtonVisibility(findViewById(R.id.startSession))
        toggleButtonVisibility(findViewById(R.id.endSession))
    }

    fun startSession() {
        val button = findViewById<Button>(R.id.startSession)
        button.isEnabled = false
        Visitor.startSession()
    }

    fun endSession() {
        val button = findViewById<Button>(R.id.endSession)
        button.isEnabled = false
        Visitor.endSession()
    }

    fun openBrowser() {
        val context = this
        val intent = Intent(context, WebView::class.java)
        startActivity(intent)
    }
}
