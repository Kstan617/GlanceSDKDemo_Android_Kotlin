# Glance Android SDK Integration
## Visitor Sessions
###### (c) 2018 Glance Networks, Inc. - All Rights Reserved

The Glance Android SDK will display the mobile application or device screen to a remote agent. Once the session
starts showing, the SDK will track view changes, activity changes, and device rotation.

This document describes the more common use case of **_Visitor_** sessions.
A Visitor session is started by a user of your app that is not an authenticated Glance user, though they may be an authenticated user of your app.  The session is joined by a customer support or sales *Agent* that is an authenticated Glance user.

See (other documentation) for authenticating users with the **User** class and starting authenticated **HostSession**s to be joined by guests who are not Glance users.


To integrate with the SDK, an application needs to take the following steps, each outlined in
their own section:
1. Get a an agent account and Group ID from Glance
2. Include the AAR
3. Initialize the Visitor class
4. Set an Event handler
5. Start a Visitor session
6. Confirm it's working
7. End the session
8. (Optional) Permissions to capture entire screen (all applications)
version is Lollipop or later, the session will request the appropriate
permissions... (_Do we have this?_)
9. (Optional) Masking Views
the remote agent.
10. (Optional) Agent Viewer Session
show a video of them to the user.
12. (Optional) Specify a Custom Agent Viewer
12. (Optional) Specify Custom Settings

### Get a an agent account and Group ID from Glance
Contact Glance at `mobilesupport@glance.net` to set up a Group (organization account) and an individual Agent account.
You will receive:
1. A numeric Group ID
2. An agent username, for example agnes.support.glance.net (Glance usernames are in the form of DNS names)
3. An agent password

In production usage, agents will typically authenticate with a single-sign-on, often via their CRM implementation (e.g. Salesforce).

### Include the AAR
To include the AAR in the app, put it in the app/libs folder.
Then modify the application’s build.gradle file to include the following just below "apply plugin:
'com.android.application'":

```
repositories {
    flatDir {
        dirs 'libs'
    }
}
```

Ensure multiDexEnabled is set to true within the android > defaultConfig section:

```
android {
    defaultConfig {
        minSdkVersion 19

        ...

        // Enabling multidex support.
        multiDexEnabled true
    }
    ...
}
```

And within the dependencies block of the build.gradle file, add the following:

```
implementation(name: 'glanceSDK', ext: 'aar')
```

Then, sync gradle by going to Tools -> Android -> Sync Project with Gradle Files or clicking the
icon. Until you do this, Android Studio won’t be able to autocomplete or import the
appropriate classes.

### Intitialize the Visitor class

You must call Visitor.init once to initialize the visitor class for your group.  Pass a reference to your main activity and your Group ID as the second parameter.

You may also pass optional name, email and phone number strings to identify your user.  This information will be stored in Glance session records and available on reports and via our APIs.
You can pass empty strings or any other values useful to you.  The limits are, name: 62 characters, email: 26 characters, phone: 30 characters.

The third argument to Visitor.init is reserved for future use, you should pass an empty string.

```java
Visitor.init(activity, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234");
```

### Set an Event handler

Put something like this in your Activity:

```kotlin
class MyActivity : VisitorListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Visitor.init(this, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234")
        Visitor.addListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        Visitor.removeListener(this)
    }

    // this is called on the Glance Event thread
    override fun onGlanceVisitorEvent(event: Event) {
        if (event.code == EventCode.EventConnectedToSession) {
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here
            // to display to the user to read to the agent
            final String sessionKey = event.GetValue("sessionkey");
        }
        else if (event.code == EventCode.EventSessionEnded) {
            // Show any UI to indicate the session has ended
        }
        else if (event.type == EventType.EventWarning ||
                event.type == EventType.EventError ||
                event.type == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
        }
    }
}
```

### Start a Visitor session

To start a session with a random key call:

```java
Visitor.startSession();
```

If you have a known value you wish to use for a key, such as a customer id, you can pass it to StartSession.  The key may only contain characters from the Base64URL set, up to 63 characters maximum.

```java
Visitor.startSession("123456789");
```

### Confirm it's working

To confirm the integration is working, start the session and note the session key.

The agent should go to:
<https://www.glance.net/agentjoin/AgentJoin.aspx> then login with their Glance username and password.  A form will be shown to enter the session key.

When the key is known a view can be opened directly with
<https://www.glance.net/agentjoin/AgentView.aspx?username={username}&sesionkey={key}&wait=1>

See (other documentation) for details on integrating the agent viewer with other systems and single-sign-on

### End the session

The session can be ended from either the agent side or the app side.  To end from the app, call:

```java
Visitor.endSession();
```

In either case the event EventSessionEnded will be sent.

### Masking Views

Specific views can be hidden from the agent by passing it them to the SDK

```java
Visitor.addMaskedView(findViewById(R.id.password_view));
```

```java
Visitor.removeMaskedView(findViewById(R.id.password_view));
```

## Support

Email mobilesupport@glance.net with any questions.
